﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.Payeer
{
    public static class PayeerPaymentDefaults
    {
        public const string SYSTEM_NAME = "Payments.Payeer";

        public static string PayeerMerchantUrl => "https://payeer.com/merchant";
    }
}
