﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Logging;
using Nop.Services.Orders;

namespace Nop.Plugin.Payments.Payeer.Services
{
    public class PayeerPaymentService : IPayeerPaymentService
    {
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IWebHelper _webHelper;
        private readonly PayeerPaymentSettings _payeerPaymentSettings;
        private readonly ILogger _logger;
        private readonly IList<string> _payeerMerchantIPs = new List<string>(){
            "185.71.65.92",
            "185.71.65.189",
            "149.202.17.210"
        };
        public PayeerPaymentService(
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            IWebHelper webHelper,
            PayeerPaymentSettings payeerPaymentSettings,
            ILogger logger)
        {
            _orderService = orderService;
            _orderProcessingService = orderProcessingService;
            _webHelper = webHelper;
            _payeerPaymentSettings = payeerPaymentSettings;
            _logger = logger;
        }

        public async Task<string> GetPayeerPaymentUrlAsync(Order order)
        {
            var mShop = _payeerPaymentSettings.MerchantId;
            var mOrderid = order.Id.ToString();
            var mAmount = $"{order.OrderTotal}";
            var mCurr = order.CustomerCurrencyCode;
            var mDesc = order.OrderGuid.ToString();
            var mKey = _payeerPaymentSettings.SecretKey;
            var mDesc64 = Base64Encode(mDesc);

            var arr = new string[] { mShop, mOrderid, mAmount, mCurr, mDesc, mKey };
            var mSign = await SHA256(string.Join(":", arr));

            var url = $"{PayeerPaymentDefaults.PayeerMerchantUrl}/?m_shop={mShop}&m_orderid={mOrderid}&m_amount={mAmount}&m_curr={mCurr}&m_desc={mDesc64}&m_sign={mSign}";

            return url;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public async Task<string> SHA256(string data)
        {
            try
            {
                var crypt = (HashAlgorithm)CryptoConfig.CreateFromName("SHA256");
                var hash = new StringBuilder();
                var crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(data));
                foreach (var theByte in crypto)
                {
                    hash.Append(theByte.ToString("X2"));
                }
                return hash.ToString();
            }
            catch (Exception e)
            {
                //add log
                await _logger.ErrorAsync(e.ToString());
            }

            return "";
        }

        public async Task<string> HandleWebhookAsync(PayeerPaymentSettings payeerPaymentSettings, HttpRequest request)
        {
            if (!_payeerMerchantIPs.Contains(_webHelper.GetCurrentIpAddress()))
                return "error";

            if (!string.IsNullOrEmpty(request.Form["m_operation_id"]) && !string.IsNullOrEmpty(request.Form["m_sign"]))
            {
                var mKey = _payeerPaymentSettings.SecretKey;

                var arr = new List<string> {
                    request.Form["m_operation_id"],
                    request.Form["m_operation_ps"],
                    request.Form["m_operation_date"],
                    request.Form["m_operation_pay_date"],
                    request.Form["m_shop"],
                    request.Form["m_orderid"],
                    request.Form["m_amount"],
                    request.Form["m_curr"],
                    request.Form["m_desc"],
                    request.Form["m_status"]
                };

                if (!string.IsNullOrEmpty(request.Form["m_params"]))
                {
                    arr.Add(request.Form["m_params"]);
                }

                arr.Add(mKey);

                var mSign = await SHA256(string.Join(":", arr));

                if (request.Form["m_sign"].Equals(mSign) && request.Form["m_status"].Equals("success"))
                {
                    //success
                    var orderId = Convert.ToInt32(request.Form["m_orderid"]);
                    var txnId = Convert.ToString(request.Form["m_operation_id"]);

                    var order = await _orderService.GetOrderByIdAsync(orderId);

                    var sb = new StringBuilder();
                    sb.AppendLine("Payeer PDT:");
                    sb.AppendLine("mc_gross: " + request.Form["m_operation_id"]);
                    sb.AppendLine("m_operation_ps: " + request.Form["m_operation_ps"]);
                    sb.AppendLine("m_operation_date: " + request.Form["m_operation_date"]);
                    sb.AppendLine("m_operation_pay_date: " + request.Form["m_operation_pay_date"]);
                    sb.AppendLine("m_shop: " + request.Form["m_shop"]);
                    sb.AppendLine("m_orderid: " + request.Form["m_orderid"]);
                    sb.AppendLine("m_amount: " + request.Form["m_amount"]);
                    sb.AppendLine("m_curr: " + request.Form["m_curr"]);
                    sb.AppendLine("m_desc: " + request.Form["m_desc"]);
                    sb.AppendLine("m_status: " + request.Form["m_status"]);

                    //order note
                    await _orderService.InsertOrderNoteAsync(new OrderNote
                    {
                        OrderId = order.Id,
                        Note = sb.ToString(),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });

                    order.AuthorizationTransactionId = txnId;
                    await _orderService.UpdateOrderAsync(order);

                    //mark order as paid
                    await _orderProcessingService.MarkOrderAsPaidAsync(order);

                    return $"{orderId}|success";
                }
            }

            return "error";
        }
    }
}
