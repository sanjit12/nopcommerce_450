﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Payments.Payeer.Services
{
    public interface IPayeerPaymentService
    {
        Task<string> GetPayeerPaymentUrlAsync(Order order);
        Task<string> HandleWebhookAsync(PayeerPaymentSettings payeerPaymentSettings, HttpRequest request);
    }
}