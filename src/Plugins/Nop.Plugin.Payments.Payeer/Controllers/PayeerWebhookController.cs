﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Payments.Payeer.Services;
using Nop.Web.Controllers;

namespace Nop.Plugin.Payments.Payeer.Controllers
{
    public class PayeerWebhookController : BasePublicController
    {
        private readonly PayeerPaymentSettings _payeerPaymentSettings;
        private readonly IPayeerPaymentService _payeerPaymentService;

        public PayeerWebhookController(
            PayeerPaymentSettings payeerPaymentSettings,
            IPayeerPaymentService payeerPaymentService)
        {
            _payeerPaymentSettings = payeerPaymentSettings;
            _payeerPaymentService = payeerPaymentService;
        }

        [HttpPost]
        public async Task<IActionResult> WebhookHandler()
        {
            var result = await _payeerPaymentService.HandleWebhookAsync(_payeerPaymentSettings, Request);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> PaymentSuccess()
        {

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> PaymentFailure()
        {
            return View();
        }
    }
}
