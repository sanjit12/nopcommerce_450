﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Payeer
{
    /// <summary>
    /// Represents settings of payeer payment plugin
    /// </summary>
    public class PayeerPaymentSettings : ISettings
    {
        public string MerchantId { get; set; }

        public string SecretKey { get; set; }

        public string AdditionalParameterEncryptionKey { get; set; }

        public int CommissionPaidBy { get; set; }

        public bool CheckUniquenessOfOrder { get; set; }

        /// <summary>
        /// Gets or sets payment transaction mode
        /// </summary>
        public TransactMode TransactMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }

        /// <summary>
        /// Gets or sets an additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }
    }
}
