﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Payments.Payeer.Models;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.Payeer.Components
{
    [ViewComponent(Name = "PayeerPayment")]
    public class PayeerPaymentViewComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/Payments.Payeer/Views/PaymentInfo.cshtml");
        }
    }
}
