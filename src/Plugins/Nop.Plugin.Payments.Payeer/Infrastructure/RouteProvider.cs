﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Payments.Payeer.Infrastructure
{
    public class RouteProvider : IRouteProvider
    {
        public int Priority => 1000;

        public void RegisterRoutes(IEndpointRouteBuilder endpointRouteBuilder)
        {
            //Webhook
            endpointRouteBuilder.MapControllerRoute("Plugin.Payments.Payeer.WebhookHandler", "payeer/status",
                 new { controller = "PayeerWebhook", action = "WebhookHandler" });

            //Webhook
            endpointRouteBuilder.MapControllerRoute("Plugin.Payments.Payeer.WebhookHandler", "payeer/success",
                 new { controller = "PayeerWebhook", action = "PaymentSuccess" });

            //Webhook
            endpointRouteBuilder.MapControllerRoute("Plugin.Payments.Payeer.WebhookHandler", "payeer/failed",
                 new { controller = "PayeerWebhook", action = "PaymentFailure" });
        }
    }
}
